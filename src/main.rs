// These are outright wrong and are forbidden to be allowed. This forces correct code.
// If there is indeed a false positive in one of these, they can still be moved to the deny section.
#![forbid(
    clippy::complexity,
    clippy::correctness,
    clippy::perf,
    clippy::style,
    const_err,
    deprecated,
    deprecated_in_future,
    arithmetic_overflow,
    exported_private_dependencies,
    future_incompatible,
    improper_ctypes,
    incomplete_features,
    invalid_value,
    irrefutable_let_patterns,
    macro_use_extern_crate,
    mutable_transmutes,
    no_mangle_const_items,
    no_mangle_generic_items,
    non_ascii_idents,
    non_shorthand_field_patterns,
    nonstandard_style,
    overflowing_literals,
    redundant_semicolons,
    renamed_and_removed_lints,
    rust_2018_compatibility,
    rust_2018_idioms,
    stable_features,
    trivial_bounds,
    trivial_casts,
    trivial_numeric_casts,
    type_alias_bounds,
    unconditional_recursion,
    unknown_crate_types,
    unknown_lints,
    unnameable_test_items,
    unreachable_pub,
    unsafe_code,
    //unused,
    unused_comparisons,
    unused_import_braces,
    unused_lifetimes,
    unused_results,
    while_true
)]
// These are denied so one is forced to annotate expected behaviour by allowing it.
#![deny(
    clippy::nursery,
    clippy::pedantic,
    clippy::restriction,
    missing_copy_implementations,
    unreachable_code,
    unused_qualifications,
    variant_size_differences
)]
// These can either not be #[allow()]'d, have false-positives or are development-tools
// of which we only care about in the CI.
#![warn(
    clippy::float_arithmetic,
    clippy::inline_always,
    clippy::integer_arithmetic,
    clippy::missing_docs_in_private_items,
    clippy::missing_inline_in_public_items,
    clippy::shadow_reuse,
    clippy::shadow_same,
    clippy::unimplemented,
    missing_debug_implementations,
    missing_docs,
    rustdoc,
    single_use_lifetimes
)]
#![allow(
    box_pointers,
    clippy::cargo,
    clippy::implicit_return,
    clippy::missing_inline_in_public_items,
    clippy::unreachable,
    meta_variable_misuse
)]
// allow these in tests
#![cfg_attr(
    test,
    allow(
        clippy::as_conversions,
        clippy::decimal_literal_representation,
        clippy::integer_arithmetic,
        clippy::let_underscore_must_use,
        clippy::option_unwrap_used,
        clippy::result_unwrap_used,
        clippy::panic,
        clippy::unnecessary_operation,
    )
)]
#![cfg_attr(test, allow(unused_results))]

//! No documentation yet.
fn main() {
    #[allow(clippy::print_stdout)]
    println!("Nothing to see here yet.")
}

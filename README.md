# Speed of Light

[![Crates.io][crates-badge]][crates-url]
[![License][license-badge]][license-url]
[![Deps.rs][deps-badge]][deps-url]
[![Build Status][build-badge]][build-url]
[![Code Coverage][coverage-badge]][coverage-url]

[crates-badge]: https://img.shields.io/crates/v/sol-game.svg
[crates-url]: https://crates.io/crates/sol-game
[license-badge]: https://img.shields.io/crates/l/sol-game
[license-url]: LICENSE
[deps-badge]: https://deps.rs/repo/gitlab/bit-refined/sol-game/status.svg
[deps-url]: https://deps.rs/repo/gitlab/bit-refined/sol-game
[build-badge]: https://gitlab.com/bit-refined/sol/badges/master/pipeline.svg
[build-url]: https://gitlab.com/bit-refined/sol/commits/master
[coverage-badge]: https://gitlab.com/bit-refined/sol/badges/master/coverage.svg
[coverage-url]: https://gitlab.com/bit-refined/sol/commits/master
